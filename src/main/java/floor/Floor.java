package floor;

/**
 * Created by Ja on 2017-08-21.
 */
public class Floor {
    private int mFloorNo;

    public Floor(int mFloorNo) {
        this.mFloorNo = mFloorNo;
    }

    public int getmFloorNo() {
        return mFloorNo;
    }

    public void setmFloorNo(int mFloorNo) {
        this.mFloorNo = mFloorNo;
    }

    @Override
    public String toString() {
        return "" + mFloorNo;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if(obj instanceof Floor){

            Floor temp = (Floor) obj;

            if(mFloorNo == temp.mFloorNo) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public int hashCode() {
        return Integer.hashCode(mFloorNo);
    }

    public void upFloor(){
        mFloorNo++;
    }

    public void downFloor(){
        mFloorNo--;
    }
}

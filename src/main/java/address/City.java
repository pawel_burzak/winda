package address;

/**
 * Created by Ja on 2017-08-22.
 */
public enum City {

    LUBLIN("Lublin"),
    WARSZAWA("Warszawa"),
    GDAŃSK("Gdańsk"),
    KATOWICE("Katowice"),
    STALOWAWOLA("Stalowa Wola"),
    ŁÓDŹ("Łódź");

    private String name;

    City(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }
}

package address.utils;

import com.sun.istack.internal.Nullable;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Ja on 2017-08-22.
 */
final public class PostalCodeUtils {

    public PostalCodeUtils() {
    }

    public static boolean isCorrect(String aCode, @Nullable String aRegion){

        boolean result = false;

        if (aRegion == null) {
            Locale locale = Locale.getDefault();
            aRegion = locale.getISO3Country();
        }

        Pattern pattern = null;

        switch (aRegion) {
            case "POL":
                pattern = Pattern.compile("\\d\\d-\\d\\d\\d");  //wyrazenia regularne
                break;
            case "USA":
                pattern = Pattern.compile("\\d{5}");
                break;
        }

        if (pattern != null) {
            Matcher matcher = pattern.matcher(aCode);
            result = matcher.matches();
        }

        return result;
    }
}

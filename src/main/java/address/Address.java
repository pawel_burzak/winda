package address;

/**
 * Created by Ja on 2017-08-22.
 */
final public class Address {

    private City mCity;
    private String mStreetName;
    private int mBlockNo;
    private int mApartmentNo;
    private String mPostalCode;

    public Address(City mCity, String mStreetName, int mBlockNo, int mApartmentNo, String mPostalCode) {
        this.mCity = mCity;
        this.mStreetName = mStreetName;
        this.mBlockNo = mBlockNo;
        this.mApartmentNo = mApartmentNo;
        this.mPostalCode = mPostalCode;
    }


    public City getCity() {
        return mCity;
    }

    public void setCity(City mCity) {
        this.mCity = mCity;
    }

    public String getStreetName() {
        return mStreetName;
    }

    public void setStreetName(String mStreetName) {
        this.mStreetName = mStreetName;
    }

    public int getBlockNo() {
        return mBlockNo;
    }

    public void setBlockNo(int mBlockNo) {
        this.mBlockNo = mBlockNo;
    }

    public int getApartmentNo() {
        return mApartmentNo;
    }

    public void setApartmentNo(int mApartmentNo) {
        this.mApartmentNo = mApartmentNo;
    }

    public String getPostalCode() {
        return mPostalCode;
    }

    public void setPostalCode(String mPostalCode) {
        this.mPostalCode = mPostalCode;
    }

    @Override
    public String toString() {
        return mCity.getName() + " " + mStreetName + " " + mBlockNo + " " + mApartmentNo + " " + mPostalCode;
    }
}

package elevator;

import block.QueueChecker;
import floor.Floor;
import person.ElevatorUser;
import weight.Weight;
import weight.WeightMetrics;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ja on 2017-08-21.
 */
public class Elevator {

    private List<Floor> mAvailableFloors;
    private List<Floor> mRequestFloors = new ArrayList<>();
    private Floor mCurrentFloor;
    private boolean mServiceBreak = false;
    protected Weight mMaxLoad = new Weight(600, WeightMetrics.KG);
    protected Direction mDirection = Direction.NONE;
    protected QueueChecker queueChecker;


    public List<Floor> getAvailableFloors() {
        return mAvailableFloors;
    }

    public void setAvailableFloors(List<Floor> mAvailableFloors) {
        this.mAvailableFloors = mAvailableFloors;
    }

    public List<Floor> getRequestFloors() {
        return mRequestFloors;
    }

    public void setRequestFloors(List<Floor> mRequestFloors) {
        this.mRequestFloors = mRequestFloors;
    }

    public Floor getCurrentFloor() {
        return mCurrentFloor;
    }

    public void setCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    public boolean isServiceBreak() {
        return mServiceBreak;
    }

    public void setServiceBreak(boolean mServiceBreak) {
        this.mServiceBreak = mServiceBreak;
    }

    public Weight getMaxLoadKg() {
        return mMaxLoad;
    }

    public void setMaxLoad(Weight mMaxLoadKg) {
        this.mMaxLoad = mMaxLoadKg;
    }

    public Direction getDirection() {
        return mDirection;
    }

    public void setDirection(Direction mDirection) {
        this.mDirection = mDirection;
    }

    public void nextStep() {

        }



    @Override
    public String toString() {
        return super.toString() + " Jestem na piętrze " + mCurrentFloor.getmFloorNo() +
                " jadę do " + getDirection().getName();
    }
}

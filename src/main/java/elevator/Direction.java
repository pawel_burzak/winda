package elevator;

/**
 * Created by Ja on 2017-08-21.
 */
public enum Direction {
    UP("góry"), DOWN("dołu"), NONE("nigdzie");

    private String name;

    Direction(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

package elevator;

import floor.Floor;
import person.ElevatorUser;
import weight.Weight;
import weight.WeightMetrics;

import java.util.ArrayList;
import java.util.List;

import static weight.Weight.SAFE_WEIGHT_FACTOR;


/**
 * Created by Ja on 2017-08-21.
 */
public class PassengerElevator extends Elevator {


    private List<ElevatorUser> mPersonsOnBoard = new ArrayList<>();
    private short mMaxLoadPerson;



    @Override
    public void setMaxLoad(Weight mMaxLoadKg) {
        super.setMaxLoad(mMaxLoadKg);

        float safeWeight = mMaxLoad.getAmount() * SAFE_WEIGHT_FACTOR;

        float typicalPersonWeight = ElevatorUser.AVERAGE_WEIGHT_OF_PERSON_KG;

        if(mMaxLoad.getmWeightMetrics() == WeightMetrics.LB){
            typicalPersonWeight *= Weight.LB_FACTOR;
        }

        mMaxLoadPerson = (short)(safeWeight / typicalPersonWeight);


    }

    @Override
    public void nextStep() {

        for(int i = 0; i < mPersonsOnBoard.size(); i++){
            if(mPersonsOnBoard.get(i).getWantToGoFloor().equals(getCurrentFloor())){
                removeUser(mPersonsOnBoard.get(i));
            }
        }

        if(mPersonsOnBoard.size() == 0){
            mDirection = Direction.NONE;
        }

       boolean anybodyGoToElevator = false;
        if(queueChecker.isEmptyQueueOnFloor(getCurrentFloor()) == false){
            List<ElevatorUser> elevatorUserList = queueChecker.getPersonsFromFloor(getCurrentFloor());

            ElevatorUser elevatorUser;
            for(int i = 0; i < elevatorUserList.size(); i++){
                elevatorUser = elevatorUserList.get(i);

                if(mDirection == Direction.NONE){
                    mDirection = elevatorUser.getDirection();
                }
                if(elevatorUser.getDirection() == mDirection){
                    mPersonsOnBoard.add(elevatorUser);
                    queueChecker.removePersonFromQueue(elevatorUser);
                    anybodyGoToElevator = true;
                }

            }
        }

        if(anybodyGoToElevator){
            return;
        }

        if(mDirection == Direction.UP){
            getCurrentFloor().upFloor();
        } else if (mDirection == Direction.DOWN){
            getCurrentFloor().downFloor();
        }


    }


    public void removeUser(ElevatorUser elevatorUser){
        mPersonsOnBoard.remove(elevatorUser);
        System.out.println("Użytkownik " + elevatorUser.getFirstName() + " wyszedł z windy.");
    }

}



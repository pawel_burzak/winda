package elevator;

import block.BlockWithElevators;
import floor.Floor;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by Ja on 2017-08-21.
 */
public class ElevatorManager {

    List<Elevator> elevators;
    private BlockWithElevators blockWithElevators;
    private AtomicBoolean isWorking = new AtomicBoolean(false);


    public ElevatorManager(BlockWithElevators blockWithElevators) {
        this.blockWithElevators = blockWithElevators;
    }

    public boolean startWork() {

            if (isStartAlready().get()) {
                return false;
            } else {

                isWorking.set(true);


                    new Thread(() -> {while(isWorking.get()) {

                        moveElevators();
                        printState();


                        try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }


                    }}).start();
                    }

                    return true;

                }

        private void printState() {
            blockWithElevators.getElevators().stream().forEach(elevator -> System.out.println(elevator.toString()));
        }


        private void moveElevators() {
            blockWithElevators.getElevators().stream().forEach(elevator -> elevator.nextStep());
        }



        public void stopWork(){
            isWorking.set(false);
        }

        public AtomicBoolean isStartAlready(){
            return isWorking;
        }


    //
//                            int currentFloor = elevator.getCurrentFloor().getmFloorNo();
//
//                            List<Floor> requestFloors = blockWithElevators.getFloors();
//
//                            int destinationFloor = requestFloors.get(0).getmFloorNo();
//
//                            if (currentFloor < destinationFloor) {
//                                while (currentFloor == destinationFloor) {
//                                    floor.upFloor();
//                                    currentFloor++;
//                                }
//                            } else {
//                                while (currentFloor == destinationFloor) {
//                                    floor.downFloor();
//                                    currentFloor--;
//                                }
//                            }

}

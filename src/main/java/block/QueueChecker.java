package block;

import floor.Floor;
import person.ElevatorUser;

import java.util.List;

/**
 * Created by Ja on 2017-08-25.
 */
public interface QueueChecker {

    boolean isEmptyQueueOnFloor(Floor floor);

    List<ElevatorUser> getPersonsFromFloor(Floor floor);

    void removePersonFromQueue(ElevatorUser elevatorUser);
}

package block;

import elevator.Elevator;
import floor.Floor;
import person.ElevatorUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Ja on 2017-08-23.
 */
public class BlockWithElevators extends Block implements QueueChecker {

    private List<Elevator> mElevators;

    private HashMap<Floor, List<ElevatorUser>> mElevatorQueue;

    public BlockWithElevators(List<Floor> mFloors) {
        super(mFloors);
        mElevatorQueue = new HashMap<>();
    }


    public List<Elevator> getElevators() {
        return mElevators;
    }

    public void setElevators(List<Elevator> mElevators) {
        this.mElevators = mElevators;
    }

    public HashMap<Floor, List<ElevatorUser>> getElevatorQueue() {
        return mElevatorQueue;
    }

    public void setElevatorQueue(HashMap<Floor, List<ElevatorUser>> mElevatorQueue) {
        this.mElevatorQueue = mElevatorQueue;
    }

    public void addElevatorUserToQueue(Floor aFloor, ElevatorUser aElevatorUser){

        List<ElevatorUser> listOfQueueOnFloor = mElevatorQueue.get(aFloor);
        //skoro mamy hashmap musimy nadpisac metode equals i hashcode zeby metoda get dobrze
        // porownywala pietra, bo kluczem jest obiekt floor (gdyby to byl integer to nie trzeba tego robic)

        if(listOfQueueOnFloor != null) {
            listOfQueueOnFloor.add(aElevatorUser);
            System.out.println("Znalezlismy pietro, dodajemy uzytkownika " + aElevatorUser.getFirstName() + " do pietra " + aFloor.getmFloorNo());
        } else {
            ArrayList<ElevatorUser> elevatorUsers = new ArrayList<>();
            elevatorUsers.add(aElevatorUser);
            mElevatorQueue.put(aFloor, elevatorUsers);
            System.out.println("Nie mamy pietra, dodajemy pietro " + aFloor.getmFloorNo() + " i uzytkownika " + aElevatorUser.getFirstName());
        }


    }

    @Override
    public boolean hasElevator() {
        return false;
    }

    @Override
    public boolean isEmptyQueueOnFloor(Floor floor) {
        return mElevatorQueue.containsKey(floor);
    }

    @Override
    public List<ElevatorUser> getPersonsFromFloor(Floor floor) {
        return mElevatorQueue.get(floor);
    }

    @Override
    public void removePersonFromQueue(ElevatorUser elevatorUser) {
        List<ElevatorUser> elevatorUserList = mElevatorQueue.get(elevatorUser.getCurrentFloor());
        elevatorUserList.remove(elevatorUser);
    }
}

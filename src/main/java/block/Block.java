package block;

import address.Address;
import floor.Floor;

import java.util.List;

/**
 * Created by Ja on 2017-08-23.
 */
public abstract class Block {

    private List<Floor> mFloors;
    private Address mAddress;

    public abstract boolean hasElevator();

    public Block(List<Floor> mFloors) {
        this.mFloors = mFloors;
    }

    public List<Floor> getFloors() {
        return mFloors;
    }

    public void setFloors(List<Floor> mFloors) {
        this.mFloors = mFloors;
    }

    public Address getAddress() {
        return mAddress;
    }

    public void setAddress(Address mAddress) {
        this.mAddress = mAddress;
    }
}

package weight;

/**
 * Created by Ja on 2017-08-21.
 */
public enum WeightMetrics {
    KG, LB
}

package weight;

/**
 * Created by Ja on 2017-08-21.
 */
public class Weight {

    public static final double LB_FACTOR = 2.2;
    public static final float SAFE_WEIGHT_FACTOR = 0.9f;
    public static final double KG_FACTOR = 0.45;

    private int aAmount;
    private WeightMetrics mWeightMetrics;

    public Weight(int aAmount, WeightMetrics aWeightMetrics) {
        this.aAmount = aAmount;
        this.mWeightMetrics = aWeightMetrics;
    }

    public int getAmount() {
        return aAmount;
    }

    public void setAmount(int aAmount) {
        this.aAmount = aAmount;
    }

    public WeightMetrics getmWeightMetrics() {
        return mWeightMetrics;
    }

    public void setmWeightMetrics(WeightMetrics mWeightMetrics) {
        this.mWeightMetrics = mWeightMetrics;
    }

    public float getWeight(WeightMetrics aWeightMetrics) {

        if(aWeightMetrics != this.mWeightMetrics && aWeightMetrics == WeightMetrics.LB) {
            return (float)(aAmount * LB_FACTOR);
        } else if (aWeightMetrics != this.mWeightMetrics && aWeightMetrics == WeightMetrics.KG){
            return (float)(aAmount * KG_FACTOR);
        }
        return aAmount;
    }

    @Override
    public String toString() {
        return aAmount + " " + mWeightMetrics;
    }
}

package person;

import address.Address;
import weight.Weight;

/**
 * Created by Ja on 2017-08-22.
 */
public class Person {

    private String FirstName;
    private String SecondName;
    private Address address;
    private Weight mWeigth;

    public Person() {
    }

    public Person(String firstName, String secondName, Address address, Weight mWeigth) {
        FirstName = firstName;
        SecondName = secondName;
        this.address = address;
        this.mWeigth = mWeigth;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String secondName) {
        SecondName = secondName;
    }

    public Weight getmWeigth() {
        return mWeigth;
    }

    public void setmWeigth(Weight mWeigth) {
        this.mWeigth = mWeigth;
    }

    @Override
    public String toString() {
        return FirstName + " " + SecondName + " " + address + " " + mWeigth;
    }
}

package person;

import elevator.Direction;
import floor.Floor;
import address.Address;
import weight.Weight;

import java.util.List;

/**
 * Created by Ja on 2017-08-21.
 */
public class ElevatorUser extends Person {

    public static final int AVERAGE_WEIGHT_OF_PERSON_KG = 90;

    private boolean isInElevator;
    private Floor mWantToGoFloor;
    private Floor mCurrentFloor;

    public ElevatorUser() {
    }

    public ElevatorUser(String firstName, String secondName, Address address, Weight mWeigth, boolean isInElevator, Floor mWantToGoFloor, Floor mCurrentFloor) {
        super(firstName, secondName, address, mWeigth);
        this.isInElevator = isInElevator;
        this.mWantToGoFloor = mWantToGoFloor;
        this.mCurrentFloor = mCurrentFloor;
    }

    public Floor getWantToGoFloor() {
        return mWantToGoFloor;
    }

    public void setWantToGoFloor(Floor mWantToGoFloor) {
        this.mWantToGoFloor = mWantToGoFloor;
    }

    public Floor getCurrentFloor() {
        return mCurrentFloor;
    }

    public void setCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    @Override
    public String toString() {
        return super.toString() + " " + isInElevator + " " + mWantToGoFloor + " " + mCurrentFloor;
    }

    public Direction getDirection() {
        int currentFloor = getCurrentFloor().getmFloorNo();
        int destinationFloor = getWantToGoFloor().getmFloorNo();
        if(currentFloor > destinationFloor){
            return Direction.DOWN;
        }
        return Direction.UP;
    }
}

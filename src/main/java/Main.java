import block.BlockWithElevators;
import elevator.Elevator;
import elevator.PassengerElevator;
import floor.Floor;
import person.ElevatorUser;
import address.Address;
import address.City;
import weight.Weight;
import weight.WeightMetrics;

import java.util.*;

/**
 * Created by Ja on 2017-08-21.
 */
public class Main {
    public static void main(String[] args) {


//        String s = "";
//        try(BufferedReader read = new BufferedReader(new FileReader("gson.txt"))){
//            s = read.readLine();
//        } catch (IOException e){
//
//        }
//
//        Gson gson = new Gson();
//        PassengerElevator elevator = gson.fromJson(s, PassengerElevator.class);
//
//
//        String out2 = gson.toJson(elevator);
//
//        System.out.println(out2);

        Weight weight = new Weight(80, WeightMetrics.LB);
////        System.out.println(weight);
////        System.out.println(weight.getWeight(WeightMetrics.KG));
//
        Address address = new Address(City.LUBLIN, "Al. Racławickie", 17, 64, "20-538");
//        Person person = new Person("Jan", "Kowalski", address, weight);
//
//        System.out.println(person);
//
        Floor destination = new Floor(4);
        Floor currentFloor = new Floor(2);
//
        ElevatorUser user = new ElevatorUser("Jan", "Kowalski", address, weight, false, destination, currentFloor);
        ElevatorUser user2 = new ElevatorUser("Jan", "Kowalski", address, weight, false, destination, currentFloor);
//        System.out.println(user);
//
//        if(PostalCodeUtils.isCorrect("20-583", "POL")){
//            System.out.println("ok");
//        }

        List<Floor> floorList = new ArrayList<>(12);
        for(int i = -2; i < 12; i++){
            Floor floor = new Floor(i);
            floorList.add(floor);
        }


        BlockWithElevators blockWithElevators = new BlockWithElevators(floorList);

        Elevator elevator = new PassengerElevator();
        elevator.setAvailableFloors(blockWithElevators.getFloors());
        elevator.setCurrentFloor(new Floor(0));
        elevator.setMaxLoad(new Weight((800), WeightMetrics.KG));

        System.out.println(elevator.toString());
        blockWithElevators.setElevators(Arrays.asList(elevator));

//        blockWithElevators.addElevatorUserToQueue(new Floor(2), user);
//        blockWithElevators.addElevatorUserToQueue(new Floor(2), user2);


        Scanner sc = new Scanner(System.in);
        String name = "";
        int currentF = 0;
        int destinationF = 0;
        String exit;
        String temp;

        do {

            System.out.println("Podaj imię:");
            name = sc.nextLine();
            System.out.println("Podaj obecne piętro:");
            temp = sc.nextLine();
            currentF = Integer.valueOf(temp);
            System.out.println("Podaj docelowe piętro:");
            temp = sc.nextLine();
            destinationF = Integer.valueOf(temp);

            ElevatorUser elevatorU = new ElevatorUser();
            elevatorU.setFirstName(name);
            elevatorU.setCurrentFloor(new Floor(currentF));
            elevatorU.setWantToGoFloor(new Floor(destinationF));

            blockWithElevators.addElevatorUserToQueue(new Floor(destinationF), elevatorU);

            System.out.println("Czy chcesz dodać klejną osobę, jeśli nie wpisz 'exit'");
            exit = sc.nextLine();


        } while(!exit.equals("exit"));


//wywolujemy metode start przed wywolywaniem petli z poborem uzytkownikow, tworzymy windy
        //napisac printliny w nextstep, musimy pobrac liste uzytkownikow na kolejnym pietrze w metodzie
        //nextstep


    }
}
